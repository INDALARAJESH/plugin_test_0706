# My GitLab Project

## Overview

This project includes a GitLab CI/CD pipeline that automatically populates merge request descriptions with the git difference using Gen AI.

## Folder Structure



## Setting Up

1. Ensure you have the following environment variables set in your GitLab CI/CD settings:
   - `GITLAB_TOKEN`: Your GitLab personal access token.
   - `GEN_AI_API_KEY`: Your Gen AI API key.

2. The `.gitlab-ci.yml` file defines the pipeline stages:
   - `fetch_diff`: Fetches the git difference and stores it in `diff.txt`.
   - `update_mr`: Processes the git difference with Gen AI and updates the MR description.

## Running the Pipeline

The pipeline is triggered automatically for merge requests. When a merge request is created, the pipeline will:
1. Fetch the git difference between the source and target branches.
2. Send the git difference to Gen AI for processing.
3. Update the merge request description with the processed information.
