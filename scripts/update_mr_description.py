import os
import time
import requests
import openai
from openai.error import RateLimitError

# Load environment variables
OPENAI_API_KEY = os.environ.get('OPENAI_API_KEY')
if not OPENAI_API_KEY:
    raise KeyError('OPENAI_API_KEY not found in environment variables')

GITLAB_TOKEN = os.environ.get('GITLAB_TOKEN')
if not GITLAB_TOKEN:
    raise KeyError('GITLAB_TOKEN not found in environment variables')

PROJECT_ID = os.environ.get('CI_PROJECT_ID')
MR_ID = os.environ.get('CI_MERGE_REQUEST_IID')

openai.api_key = OPENAI_API_KEY

def get_diff():
    with open('diff.txt', 'r') as file:
        return file.read()

def process_diff_with_openai(diff, retries=3, delay=60):
    for attempt in range(retries):
        try:
            response = openai.ChatCompletion.create(
                model="gpt-3.5-turbo",
                messages=[{"role": "user", "content": f"Summarize the following git diff:\n\n{diff}"}],
                temperature=0.5,
            )
            return response.choices[0]['message']['content']
        except RateLimitError as e:
            print(f"RateLimitError: {e}. Retrying in {delay} seconds... (Attempt {attempt + 1}/{retries})")
            time.sleep(delay)
    raise Exception("Exceeded maximum retries for OpenAI API due to rate limit.")

def update_merge_request_description(description):
    url = f'https://gitlab.com/api/v4/projects/{PROJECT_ID}/merge_requests/{MR_ID}'
    headers = {'PRIVATE-TOKEN': GITLAB_TOKEN}
    data = {'description': description}
    response = requests.put(url, headers=headers, json=data)
    if response.status_code != 200:
        print(f"Failed to update merge request: {response.status_code}, {response.text}")

if __name__ == '__main__':
    diff = get_diff()
    if not diff:
        print("No changes detected in diff.txt.")
    else:
        description = process_diff_with_openai(diff)
        update_merge_request_description(description)
